// ����������� ����������
#include <cmath>
#include <cstdio>
#include <ctime>

// ���������������� ����������
#include <SFML/Graphics.hpp>

double eps;   // ����������� epsilon � ��������� Verlet-Like
double m = 1; // ����� ������

double dt; // ��� �� �������
int N;     // ����� ������ � ������� (N - ������ �����)

double* V;    // �������� v(t) �� ����� ����
double* newV; // �������� v(t) �� ������ ����
double* Q;    // �������� q(t) �� ����� ����
double* newQ; // �������� q(t) �� ������ ����
double* A;    // �������� a(t) �� ����� ����
double* newA; // �������� a(t) �� ������ ����

double alp; // ����������� alpha
double b;   // ����������� beta

// �������, ������������� �������� -grad V (��� -V~) ��� V = r^2 / 2
void CountAccHarm(double* q, double* a)
{
	for (int i = 1; i < N - 1; i++)
		a[i] = 1.0 / m * (q[i + 1] - 2 * q[i] + q[i - 1]);

	a[0] = 1.0 / m * (q[1] - 2 * q[0] + q[N - 1]);
	a[N - 1] = 1.0 / m * (q[0] - 2 * q[N - 1] + q[N - 2]);
}

// �������, ������������� �������� -grad V (��� -V~) ��� FPU-����������
void CountAccFPU(double* q, double* a)
{
	double temp;
	double summ;
	for (int i = 1; i < N - 1; i++)
	{
		summ = 0.0;
		temp = q[i + 1] - q[i];
		summ += alp * temp * temp;
		summ += b * temp * temp * temp;
		temp = q[i] - q[i - 1];
		summ -= alp * temp * temp;
		summ -= b * temp * temp * temp;
		summ += (q[i + 1] - 2.0 * q[i] + q[i - 1]);

		a[i] = 1.0 / m * summ;
	};

	summ = 0.0;
	temp = q[1] - q[0];
	summ += alp * temp * temp;
	summ += b * temp * temp * temp;
	temp = q[0] - q[N - 1];
	summ -= alp * temp * temp;
	summ -= b * temp * temp * temp;
	summ += (q[1] - 2.0 * q[0] + q[N - 1]);

	a[0] = 1.0 / m * summ;

	summ = 0.0;
	temp = q[0] - q[N - 1];
	summ += alp * temp * temp;
	summ += b * temp * temp * temp;
	temp = q[N - 1] - q[N - 2];
	summ -= alp * temp * temp;
	summ -= b * temp * temp * temp;
	summ += (q[0] - 2.0 * q[N - 1] + q[N - 2]);

	a[N - 1] = 1.0 / m * summ;
}

// �������� Verlet
void CountStepVerle(bool isFPU)
{
	double k; // ����������� v(t + 0.5 * dt)

			  // ������������ q(t + dt) � v(t + 0.5 * dt)
	for (int i = 0; i < N; i++)
	{
		k = V[i] + 0.5 * A[i] * dt;
		newQ[i] = Q[i] + k * dt;
		newV[i] = k;
	};

	// ������������ a(t + dt)
	if (isFPU)
		CountAccFPU(newQ, newA);
	else
		CountAccHarm(newQ, newA);

	// ������������ v(t + dt)
	for (int i = 0; i < N; i++)
		V[i] = newV[i] + 0.5 * newA[i] * dt;

	double* temp = newV;
	newV = V;
	V = temp;
}

// �������� Simpletic Verlet-Like
void CountStepVerleLike(bool isFPU)
{
	// ����������� q1(t + dt)
	const double c1 = eps * dt;
	for (int i = 0; i < N; i++)
		newQ[i] = Q[i] + V[i] * c1;

	// ������������ f(q1)
	if (isFPU)
		CountAccFPU(newQ, newV);
	else
		CountAccHarm(newQ, newV);

	// ������������ v1(t) � q2(t)
	const double c = (1.0 - 2.0 * eps) * dt;
	const double c2 = dt * 0.5 / m;
	for (int i = 0; i < N; i++)
	{
		newV[i] *= c2;
		newV[i] += V[i];
		Q[i] = newQ[i] + newV[i] * c;
	};

	// ������������ f(q2)
	if (isFPU)
		CountAccFPU(Q, V);
	else
		CountAccHarm(Q, V);

	// ������������ v(t + dt), q(t + dt)
	for (int i = 0; i < N; i++)
	{
		V[i] *= c2;
		V[i] += newV[i];
		newQ[i] = Q[i] + V[i] * c1;
	};

	// q(t + dt) "��������", ��������� v(t + dt)
	double* temp = newV;
	newV = V;
	V = temp;
}

// �������� FPU-����������
double FPU(double a)
{
	return a * a / 2.0 + alp * a * a * a / 3.0 + b * a * a * a * a / 4.0;
}

int main()
{
	int stepsAmount = 50; // ���������� ������������ ����� (��������� 1 ���� � � (100 * 0.01 � = 1 �))

	// ������������� ��������� ��������
	alp = 0.0;
	b = 1700.0;
	dt = 0.01;
	N = 500;
	
	bool isFPU = true; // ����������, ������������ ��������� � ��������
	bool isVV = false;  // ����������, ������������ �������� �������

	// ��������� ������ ��� �������
	V = new double[N];
	newV = new double[N];
	Q = new double[N];
	newQ = new double[N];
	A = new double[N];
	newA = new double[N];

	// ����������� ������������ epsilon 
	eps = 0.5 - pow(2.0 * sqrt(326.0) + 36.0, 1.0 / 3.0) / 12.0;
	eps = 1.0 / 6.0 / pow(2.0 * sqrt(326.0) + 36.0, 1.0 / 3.0);

	// ��������� ������������� q(t), a(t), V(t)
	for (int i = 0; i < N; i++)
	{
		Q[i] = 0;
		A[i] = 0;
		V[i] = 0;
	};

	const int winSize = 1200;    // ������ ����������� ����
	const float offset = 16.0f; // ������ ����� � ����
	const float recSize = 2.0f; // ������ �����

	double P0 = 1.0;   // ��������� ������� �������
	V[N / 2] = P0 / m; // ����������� ��������� ��������� ������
	//V[N / 2 - 1] = -P0 / m;

	// ������������� ����
	sf::RenderWindow window(sf::VideoMode(winSize, winSize), L"������������� ��������� ������");

	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
		}
		window.clear(sf::Color::Black); // ������������� ���� ������� �����

		double* temp; // ��������� ����������

		// ��������� ������
		for (int i = 0; i < stepsAmount; i++)
		{
			if (isVV)
				CountStepVerle(isFPU);
			else
				CountStepVerleLike(isFPU);

			temp = Q;
			Q = newQ;
			newQ = temp;

			temp = V;
			V = newV;
			newV = temp;

			temp = A;
			A = newA;
			newA = temp;
		};

		//sf::VertexArray lines(sf::Lines, N);
		for (int i = 0; i < N; i++)
		{
			//lines[i].position = sf::Vector2f(offset + i * recSize, offset + 250.0 + 150.0 * (float)V[i] * recSize);
			sf::RectangleShape rec(sf::Vector2f(recSize, recSize));
			rec.setFillColor(sf::Color::Green);
			rec.setOrigin(sf::Vector2f(-offset, -offset - 250.0));
			rec.setPosition(sf::Vector2f(i * recSize, 100.0 * (double)V[i] * recSize));
			window.draw(rec);
		};
		//window.draw(lines);
		
		window.display();
		
		//sf::sleep(sf::milliseconds(200));
	}

	// ������� ������, ���������� ��� �������
	delete(V);
	delete(newV);
	delete(Q);
	delete(newQ);
	delete(A);
	delete(newA);

	return 0;
}