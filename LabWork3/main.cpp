// Стандартные библиотеки
#include <cmath>
#include <cstdio>
#include <ctime>

double eps;   // Коэффициент epsilon в алгоритме Verlet-Like
double m = 1; // Масса частиц

double dt; // Шаг по времени
int N;     // Число частиц в цепочке (N - чётное число)

double* V;    // Значение v(t) на новом шаге
double* newV; // Значение v(t) на старом шаге
double* Q;    // Значение q(t) на новом шаге
double* newQ; // Значение q(t) на старом шаге
double* A;    // Значение a(t) на новом шаге
double* newA; // Значение a(t) на старом шаге

double alp; // Коэффициент alpha
double b;   // Коэффициент beta

// Функция, расчитывающая значение -grad V (или -V~) при V = r^2 / 2
void CountAccHarm(double* q, double* a)
{
	for (int i = 1; i < N - 1; i++)
		a[i] = 1.0 / m * (q[i + 1] - 2 * q[i] + q[i - 1]);
	
	a[0    ] = 1.0 / m * (q[1] - 2 * q[0    ] + q[N - 1]);
	a[N - 1] = 1.0 / m * (q[0] - 2 * q[N - 1] + q[N - 2]);
}

// Функция, расчитывающая значение -grad V (или -V~) при FPU-потенциале
void CountAccFPU(double* q, double* a)
{
	double temp;
	double summ;
	for (int i = 1; i < N - 1; i++)
	{
		summ = 0.0;
		temp = q[i + 1] - q[i];
		summ += alp * temp * temp;
		summ += b * temp * temp * temp;
		temp = q[i] - q[i - 1];
		summ -= alp * temp * temp;
		summ -= b * temp * temp * temp;
		summ += (q[i + 1] - 2.0 * q[i] + q[i - 1]);

		a[i] = 1.0 / m * summ;
	};

	summ = 0.0;
	temp = q[1] - q[0];
	summ += alp * temp * temp;
	summ += b * temp * temp * temp;
	temp = q[0] - q[N - 1];
	summ -= alp * temp * temp;
	summ -= b * temp * temp * temp;
	summ += (q[1] - 2.0 * q[0] + q[N - 1]);

	a[0] = 1.0 / m * summ;

	summ = 0.0;
	temp = q[0] - q[N - 1];
	summ += alp * temp * temp;
	summ += b * temp * temp * temp;
	temp = q[N - 1] - q[N - 2];
	summ -= alp * temp * temp;
	summ -= b * temp * temp * temp;
	summ += (q[0] - 2.0 * q[N - 1] + q[N - 2]);

	a[N - 1] = 1.0 / m * summ;
}

// Алгоритм Verlet
void CountStepVerle(bool isFPU)
{
	double k; // Коэффициент v(t + 0.5 * dt)
	
	// Рассчитываем q(t + dt) и v(t + 0.5 * dt)
	for (int i = 0; i < N; i++)
	{
		k = V[i] + 0.5 * A[i] * dt;
		newQ[i] = Q[i] + k * dt;
		newV[i] = k;
	};

	// Рассчитываем a(t + dt)
	if (isFPU)
		CountAccFPU(newQ, newA);
	else
		CountAccHarm(newQ, newA);

	// Рассчитываем v(t + dt)
	for (int i = 0; i < N; i++)
		V[i] = newV[i] + 0.5 * newA[i] * dt;

	double* temp = newV;
	newV = V;
	V = temp;
}

// Алгоритм Simpletic Verlet-Like
void CountStepVerleLike(bool isFPU)
{
	// Расчитываем q1(t + dt)
	const double c1 = eps * dt;
	for (int i = 0; i < N; i++)
		newQ[i] = Q[i] + V[i] * c1;

	// Рассчитываем f(q1)
	if (isFPU)
		CountAccFPU(newQ, newV);
	else
		CountAccHarm(newQ, newV);

	// Рассчитываем v1(t) и q2(t)
	const double c = (1.0 - 2.0 * eps) * dt;
	const double c2 = dt * 0.5 / m;
	for (int i = 0; i < N; i++)
	{
		newV[i] *= c2;
		newV[i] += V[i];
		Q[i] = newQ[i] + newV[i] * c;
	};

	// Рассчитываем f(q2)
	if (isFPU)
		CountAccFPU(Q, V);
	else
		CountAccHarm(Q, V);

	// Рассчитываем v(t + dt), q(t + dt)
	for (int i = 0; i < N; i++)
	{
		V[i] *= c2;
		V[i] += newV[i];
		newQ[i] = Q[i] + V[i] * c1;
	};

	// q(t + dt) "обновили", обновляем v(t + dt)
	double* temp = newV;
	newV = V;
	V = temp;
}

// Значение FPU-потенциала
double FPU(double a)
{
	return a * a / 2.0 + alp * a * a * a / 3.0 + b * a * a * a * a / 4.0;
}

int main()
{
	int stepsAmount = 10000000; // Количество итерационных шагов

	// Инициализация начальных значений
	alp = 0.0;
	b = 0.65;
	dt = 0.01;
	N = 500;
  
	// Выделение памяти под массивы
	V = new double[N];
	newV = new double[N];
	Q = new double[N];
	newQ = new double[N];
	A = new double[N];
	newA = new double[N];
	
	// Определение коэффициента epsilon 
	eps = 0.5 - pow(2.0 * sqrt(326.0) + 36.0, 1.0 / 3.0) / 12.0;
    eps = 1.0 / 6.0 / pow(2.0 * sqrt(326.0) + 36.0, 1.0 / 3.0);
	
	// Начальная инициализация q(t), a(t), V(t)
	for (int i = 0; i < N; i++)
	{
		Q[i] = 0;
		A[i] = 0;
		V[i] = 0;
	};

	double P0 = 1.0;   // Начальный импульс системы
	V[N / 2] = P0 / m; // Определение начальных скоростей частиц
	// V[N / 2 - 1] = -P0 / m;

	bool isFPU = false;  // Переменная, определяющая потенциал в расчётах
	bool isVV = false;    // Переменная, определяющая алгоритм расчёта

	double summImp = 0;  // Суммарный импульс
	double summEnrg = 0; // Суммарная энергия

	// Подсчёт суммарной энергии для FPU-потенциала
	for (int i = 0; i < N; i++)
	{
		double v;
		if (i != N - 1)
			v = Q[i + 1] - Q[i];
		else
			v = Q[0] - Q[N - 1];

		if (isFPU)
			summEnrg += v * v * 0.5 + v * v * v * alp / 3.0 + v * v * v * v * b * 0.25;
		else
			summEnrg += v * v * 0.5;
		
		summEnrg += m * V[i] * V[i] * 0.5;
	};

	double tstart, tstop; // Точки отсчёта времени выполнения программы
	tstart = (double)clock() / CLOCKS_PER_SEC; // Отмечаем начало расчёта

	int progress = 0; // Счётчик для вывода промежуточных данных
	double* temp;     // Временная переменная
	
	// Численный расчёт
	for (int i = 0; i < stepsAmount; i++)
	{
		if (isVV)
			CountStepVerle(isFPU);
		else
			CountStepVerleLike(isFPU);
		
		temp = Q;
		Q = newQ;
		newQ = temp;

		temp = V;
		V = newV;
		newV = temp;

		temp = A;
		A = newA;
		newA = temp;

		if (i / (stepsAmount / 100) > progress)
		{
			progress = i / (stepsAmount / 100);
			printf("[%i/100] \n", progress);
		};
	};

	tstop = (double)clock() / CLOCKS_PER_SEC; // Отмечаем конец расчёта

	// Вывод отладочной информации
	printf("It took %f seconds for %i time steps\n", tstop - tstart, stepsAmount);
	printf("Starting impulse 1.0, starting energy %f \n", summEnrg);

	// Сбрасываем подсчитанные импульс и энергию
	summImp = 0;
	summEnrg = 0;

	// Пересчитываем суммарный импульс и суммарную энергию для alpha-FPU V(r) = r^2 / 2 + beta * r^4 / 3
	for (int i = 0; i < N; i++)
	{
		summImp += m * V[i];

		double v;
		if (i != N - 1)
		  v = Q[i + 1] - Q[i];
		else
		  v = Q[0] - Q[N - 1];

		if (isFPU)
		  summEnrg += v * v * 0.5 + v * v * v * alp / 3.0 + v * v * v * v * b * 0.25;
		else
		  summEnrg += v * v * 0.5;

		summEnrg += m * V[i] * V[i] * 0.5;
	};
	
	// Вывод результатов на экран
	printf("Summ impulse %.16f\nSumm energy %.16f\n", summImp, summEnrg);

	// Очистка памяти, выделенной под массивы
	delete(V);
	delete(newV);
	delete(Q);
	delete(newQ);
	delete(A);
	delete(newA);

	return 0;
};