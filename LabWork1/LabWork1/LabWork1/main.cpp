#include <iostream> // ��� �����/������ ������ 
#include <cstdlib> // ��� ������� atoi(),atof()
#include <cmath> // ��� ������� sqrt()

#include <vector>
#include <string>
#include <fstream>

#include <Windows.h> // ��� ������� Sleep()

std::string output = "Output.txt";
std::string rOutput = "rData.txt";

void bifurc(double r)
{
	std::ofstream file(output, std::ios::app);
	std::ofstream rData(rOutput, std::ios::app);

	double N0 = 0.7; // ��������� �������� ����������� ���������
	double alpha = 3.0; // ����������� ������������������ � ������ ����������
	//double r = 1 + sqrt(6) + 0.1; // ����������� r � ������������� �����������

	double dt = (r - 1) / alpha; // ��� �� ������� � ������ ������
	double gamma = (1 + alpha * dt) / alpha / dt; // ����������� � ������ N_k(t) �� x_k(t)

	double x_k = N0 / gamma;
	
	// ���������� ������ 40000 �����
	for (int i = 0; i < 40000; i++)
		x_k = r * x_k * (1 - x_k);
	
	// ���������� ��������� 64 �����
	const int N = 64;
	double answers[N];
	for (int i = 0; i < N; i++)
	{
		x_k = r * x_k * (1 - x_k);
		answers[i] = x_k;
	};

	// ������� ������� �����
	for (int i = 0; i < N; i++)
		for (int j = i + 1; j < N; j++)
		{
			if (answers[i] <= 0.0)
				continue;

			if (abs(answers[i] - answers[j]) < 0.000001)
				answers[j] = -1.0;
		};

	std::cout.precision(6);
	file.precision(6);
	rData.precision(6);

	// ������� ������� �����
	int countPoint = 0;
	for (int i = 0; i < N; i++)
		if (answers[i] >= 0.0)
		{
			file << r << "\t" << answers[i] << std::endl;
			countPoint++;
		};

	std::cout << r << "\t" << countPoint << std::endl;
	rData << r << "\t" << countPoint << std::endl;

	rData.close();
	file.close();
};

int main(int argc, char *argv[])
{
	for (double r = 3.0000; r < 4; r += 1.0E-4)
		bifurc(r);

	return 0;
}