reset

#set terminal wxt persist
set terminal png size 1024, 1024
set output 'Bifurc.png'

set xlabel "Бифуркационный параметр r"
set ylabel "Значения x_k"

plot "Output.txt" using 1:2 with points notitle