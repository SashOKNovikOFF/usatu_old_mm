reset

set terminal wxt persist

plot "50.txt" using 1:2 with lines, \
     "100.txt" using 1:2 with lines, \
     "200.txt" using 1:2 with lines, \
     "400.txt" using 1:2 with lines, \
     "800.txt" using 1:2 with lines, \
     "1600.txt" using 1:2 with lines, \
     "3200.txt" using 1:2 with lines