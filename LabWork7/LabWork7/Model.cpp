#pragma once

#include "Model.hpp"

#include <ctime>
#include <cstdlib>
#include <queue>
#include <algorithm>

Model::Model(int numCells_, double pore_)
{
	numCells = numCells_;
	pore     = pore_;

	numClusters = 1;
	numConClust = 0;

	// ������������ ��������� ������ ��� ���������� ��������
	grid = new int*[numCells + 2];
	BFSgrid = new int*[numCells + 2];
	for (int index = 0; index < numCells + 2; index++)
	{
		grid[index] = new int[numCells + 2];
		BFSgrid[index] = new int[numCells + 2];
	};

	// �������� �������
	for (int x = 0; x < numCells + 2; x++)
		for (int y = 0; y < numCells + 2; y++)
		{
			grid[x][y] = 0;
			BFSgrid[x][y] = numCells * numCells + 1;
		};
}

Model::~Model()
{
	for (int index = 0; index < numCells + 2; index++)
	{
		delete grid[index];
		delete BFSgrid[index];
	};

	delete grid;
	delete BFSgrid;
}

void Model::findClusters()
{
	// ����� �� ���� ������� ������� ���������
	for (int x = 1; x <= numCells; x++)
		for (int y = 1; y <= numCells; y++)
		{
			// "��������" ����� ��������� �������
			if (grid[x][y] == 1)
			{
				numClusters++;
				grid[x][y] = numClusters;

				// �������� ������ � ������

				Point front;

				BFSgrid[x][y] = 0;
				std::queue<Point> points;
				points.push(Point(x, y));
				while (!points.empty())
				{
					front = points.front();
					points.pop();

					std::vector<Point> neighbors;
					if (grid[front.x][front.y - 1]) neighbors.push_back(Point(front.x, front.y - 1));
					if (grid[front.x + 1][front.y]) neighbors.push_back(Point(front.x + 1, front.y));
					if (grid[front.x][front.y + 1]) neighbors.push_back(Point(front.x, front.y + 1));
					if (grid[front.x - 1][front.y]) neighbors.push_back(Point(front.x - 1, front.y));

					for (std::size_t i = 0; i < neighbors.size(); i++)
						if (BFSgrid[neighbors[i].x][neighbors[i].y] == (numCells * numCells + 1))
						{
							grid[neighbors[i].x][neighbors[i].y] = numClusters;

							points.push(neighbors[i]);
							BFSgrid[neighbors[i].x][neighbors[i].y] = BFSgrid[front.x][front.y] + 1;
						};
				};

				// ���� ������ "�� �������", �������� ��� "�����"
				if (BFSgrid[front.x][front.y] == 0)
				{
					grid[x][y]    = -1;
					BFSgrid[x][y] = numCells * numCells + 1;

					numClusters--;
				}
				else if (BFSgrid[front.x][front.y] == 1)
				{
					grid[x][y]             = -1;
					grid[front.x][front.y] = -1;
					BFSgrid[x][y]             = numCells * numCells + 1;
					BFSgrid[front.x][front.y] = numCells * numCells + 1;

					numClusters--;
				};
			};
		};
}

void Model::findConClust()
{
	std::vector<int> up, down;
	for (int x = 1; x <= numCells; x++)
	{
		if (grid[x][1] > 0)
			up.push_back(grid[x][1]);
		if (grid[x][numCells] > 0)
			down.push_back(grid[x][numCells]);
	};

	if ((up.size() == 0) || (down.size() == 0))
		return;

	std::sort(up.begin(), up.end());
	std::sort(down.begin(), down.end());

	if (std::binary_search(down.begin(), down.end(), up[0]))
		numConClust++;

	for (std::size_t i = 1; i < up.size(); i++)
		if ((up[i] != up[i - 1]) && (std::binary_search(down.begin(), down.end(), up[i])))
			numConClust++;
}

int** Model::getGrid()
{
	return grid;
}

int ** Model::getBFSGrid()
{
	return BFSgrid;
}

int Model::getNumCells()
{
	return numCells;
}

int Model::getNumClusters()
{
	return numClusters - 1;
}

int Model::getNumConClust()
{
	return numConClust;
}

void Model::randomInit()
{
	for (int x = 1; x <= numCells; x++)
		for (int y = 1; y <= numCells; y++)
		{
			double temp = (double)(rand()) / (double)(RAND_MAX);
			grid[x][y] = (temp > pore) ? 1 : 0;
		};
}

void Model::userInit()
{
	grid[1][2] = 1;
	grid[2][2] = 1;

	grid[1][5] = 1;
	grid[2][5] = 1;
	grid[3][5] = 1;
	grid[3][4] = 1;
	grid[4][4] = 1;
	grid[4][3] = 1;
	grid[4][2] = 1;
	grid[4][1] = 1;
}