#pragma once

struct Point; // ���������� ������
class Model;  // ������ ���������� �������� "�����"

struct Point
{
	// ������������
	Point() : x(0), y(0) { };
	Point(int x_, int y_) : x(x_), y(y_) { };

	int x; // ���������� x
	int y; // ���������� y
};

class Model
{
private:
	int numCells;    // ���������� ������

	double pore;     // ����������� "����������"

	int numClusters; // ���������� ���������
	int numConClust; // ���������� ������� ���������

	int** grid;      // ����� ������� ���������
	int** BFSgrid;   // ����� ������ ���������

public:
	Model(int numCells_, double pore_); // �����������
	~Model();                           // ����������

	void findClusters(); // ��������� � ������� ���������
	void findConClust(); // ��������� ������� ��������
	int** getGrid();     // �������� ��������� �� ������ ������
	int** getBFSGrid();  // �������� ��������� �� ������ ������ ������
	int getNumCells();   // �������� ������ �����

	int getNumClusters(); // �������� ���������� ���������
	int getNumConClust(); // �������� ���������� ������� ���������
	
	void randomInit(); // ��������� ����� ��������� �������
	void userInit();   // ��������� ����� ��������� �������
};