reset

set terminal wxt persist

plot "0.100000.txt" using 2:1 with lines, \
     "0.050000.txt" using 2:1 with lines, \
     "0.010000.txt" using 2:1 with lines, \
     "0.005000.txt" using 2:1 with lines, \
     "0.001000.txt" using 2:1 with lines, \
     "0.000500.txt" using 2:1 with lines, \
     "0.000100.txt" using 2:1 with lines