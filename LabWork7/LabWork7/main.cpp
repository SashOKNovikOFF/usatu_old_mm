#include <SFML/Graphics.hpp>

#include "Model.hpp"
#include "printOut.hpp"

#include <iostream>
#include <vector>
#include <string>

bool DEBUG = false;                         // ����������-���� ��� ������ ���������� ����������
bool P1P2_DEBUG = true;                     // ����������-���� ��� ������ ���������� ���������� �� ������� ����������
void debugOutput(int** grid, int numCells); // ����� ������ �� ����� � �������

void poreSizeGrid(); // ������ ����������� ������ ���������� �� ������� �����
void poreRepeats();  // ������ ����������� ������ ���������� �� ����� ������������� �������

void outputClusters(int numCells, double pore);                                   // ����� ��������� �� �����
void calcClusters(int numCells, double pore, int& numClusters, int& numConClust); // ������� ��������� � ������� ���������
int getNumConClust(int numCells, double pore);                                    // ������� ������� ���������
double DichotomyMethod(int numCells, double a, double b, double epsilon);         // ����� ���������
double calcPore(int numCells, int& numClusters, int& numConClust);                // ������ ������ ����������

int numCells = 32;
const int numIter = 500;
const double deltaPore = 0.05;
const double deltaDich = 0.01;

int main()
{
	srand((unsigned int)time(NULL)); // "���������� ������"

	std::vector<double> pore;
	std::vector<double> repeats;
	std::vector<double> numClusters;

	/*for (int repeat = 50; repeat <= 300; repeat += 50)
	{
		int clusters = 0;
		int fictive = 0;
		double truePore = calcPore(numCells, clusters, fictive);

		pore.push_back(truePore);
		repeats.push_back(repeat);
		numClusters.push_back(clusters);
	};*/

	for (int repeat = 10000; repeat <= 15000; repeat += 1000)
	{
		int clusters = 0;
		int fictive = 0;
		double truePore = calcPore(numCells, clusters, fictive);

		pore.push_back(truePore);
		repeats.push_back(repeat);
		numClusters.push_back(clusters);
	};

	std::vector< std::vector<double> > v = { repeats, pore, numClusters };
	printOutWithoutHeader("M1.txt", 5, v, false);

	return 0;
}

void poreSizeGrid()
{
	std::vector<std::string> headers = { "N", "PORE", "CLUSTER" };
	std::vector<double> N;
	std::vector<double> pore;
	std::vector<double> cluster;

	for (int i = numCells; i <= 256; i *= 2)
	{
		std::cout << "NumCells: " << i << std::endl;

		int numClusters = 0;
		int numConClust = 0;
		double truePore = calcPore(i, numClusters, numConClust);

		N.push_back((double)i);
		pore.push_back(truePore);
		cluster.push_back((double)numClusters);
	};

	std::vector< std::vector<double> > v = { N, pore, cluster };
	printOut(std::to_string(numIter) + ".txt", 5, headers, v, true, false);
};

void poreRepeats()
{
	int count = 0;
	std::vector<double> pore;
	std::vector<double> repeats;

	for (double dx = 0.0; dx < 1.0; dx += deltaPore)
	{
		pore.push_back(dx);
		repeats.push_back(getNumConClust(numCells, dx));
	};

	std::vector< std::vector<double> > v = { pore, repeats };
	printOutWithoutHeader(std::to_string(deltaPore) + ".txt", 5, v, false);
};

double calcPore(int numCells, int& numClusters, int& numConClust)
{
	int count;
	double p1, p2;

	count = 0;
	do
	{
		count++;
	} while (!(getNumConClust(numCells, count * deltaPore) == 0));
	p1 = count * deltaPore;

	count = 0;
	do
	{
		count++;
	} while (getNumConClust(numCells, 1.0 - count * deltaPore) == 0);
	p2 = 1.0 - count * deltaPore;

	if (p1 > p2)
	{
		double temp = p2;
		p2 = p1;
		p1 = p2;
	};

	if (P1P2_DEBUG)
	{
		std::cout << "Left border p1: " << p1 << std::endl;
		std::cout << "Right border p2: " << p2 << std::endl;
	};

	if (DEBUG)
	{
		int n1, n2;
		n1 = getNumConClust(numCells, 1.0 - count       * deltaPore);
		n2 = getNumConClust(numCells, 1.0 - (count - 1) * deltaPore);

		std::cout << "(calcPore) A: " << 1.0 - count       * deltaPore << "; Clusters: " << n1 << std::endl;
		std::cout << "(calcPore) B: " << 1.0 - (count - 1) * deltaPore << "; Clusters: " << n2 << std::endl;
	};

	//double pore = (p1 + p2) / 2.0;
	//double pore = 1.0 - count * deltaPore;
	double pore = DichotomyMethod(numCells, p1, p2, deltaDich);
	calcClusters(numCells, pore, numClusters, numConClust);

	return pore;
};

void debugOutput(int** grid, int numCells)
{
	for (int x = 0; x < numCells + 2; x++)
	{
		for (int y = 0; y < numCells + 2; y++)
			std::cout << grid[x][y] << "\t";
		std::cout << std::endl;
	}
};

void outputClusters(int numCells, double pore)
{
	Model model(numCells, pore);        // ������
	model.randomInit();                 // ������������� ������
	model.findClusters();               // ����� ���� ���������
	model.findConClust();               // ����� ������� ���������

	int** grid = model.getGrid();       // ����� ����� ������ � "��������"

	const int winSize = 800;    // ������ ����������� ����
	const float offset = 16.0f; // ������ ����� � ����
	const float recSize = 3.0f; // ������ �����

	if (DEBUG) debugOutput(grid, numCells);

	// ������������� ����
	sf::RenderWindow window(sf::VideoMode(winSize, winSize), L"������������� ������� ����������");

	// ���� ���� �������, ������� ����� �� �����
	while (window.isOpen())
	{
		// ������������� �������
		sf::Event event;
		while (window.pollEvent(event)) // ���������� ������� "���� �������"
		{
			if (event.type == sf::Event::Closed)
				window.close();
		}

		window.clear(sf::Color::Black); // ������������� ���� ������� �����

										// ������������ ��������� ������ �����
		for (int x = 1; x <= numCells; x++)
			for (int y = 1; y <= numCells; y++)
				if (grid[x][y] != 0)
				{
					sf::RectangleShape rec(sf::Vector2f(recSize, recSize));
					if (grid[x][y] == -1)
						rec.setFillColor(sf::Color(0, 0, 255, 255));
					else
						rec.setFillColor(sf::Color(0, grid[x][y] * 50 % 255, 0, 255));
					rec.setOrigin(sf::Vector2f(-offset, -offset));
					rec.setPosition(sf::Vector2f((x - 1) * recSize, (y - 1) * recSize));
					window.draw(rec);
				};

		window.display(); // ������� ����� �� �����
	};
};

void calcClusters(int numCells, double pore, int& numClusters, int& numConClust)
{
	Model model(numCells, pore);
	model.randomInit();
	model.findClusters();
	model.findConClust();

	numClusters = model.getNumClusters();
	numConClust = model.getNumConClust();
};

int getNumConClust(int numCells, double pore)
{
	int fictiveVar;
	int numConClust;
	int counterBorder = 0;

	for (int i = 0; i < numIter; i++)
	{
		calcClusters(numCells, pore, fictiveVar, numConClust);
		if (numConClust > 0)
			counterBorder++;
	};

	return counterBorder;
};

double DichotomyMethod(int numCells, double a, double b, double epsilon)
{
	int count = 0;
	double ak = a;
	double bk = b;
	double ck = (ak + bk) / 2.0;

	while (std::abs(bk - ak) > epsilon)
	{

		double yk = (ak + ck) / 2.0;
		if (getNumConClust(numCells, yk) <= getNumConClust(numCells, ck))
		{
			bk = ck;
			ck = yk;
		}
		else
		{
			double zk = (bk + ck) / 2.0;
			if (getNumConClust(numCells, ck) <= getNumConClust(numCells, zk))
			{
				ak = yk;
				bk = zk;
			}
			else
			{
				ak = ck;
				ck = zk;
			};
		};

		count++;
	};

	if (DEBUG)
	{
		std::cout << "AK: " << ak << " F1: " << getNumConClust(numCells, ak) << std::endl;
		std::cout << "BK: " << bk << " F2: " << getNumConClust(numCells, bk) << std::endl;
		std::cout << "(AK + BK) / 2: " << (ak + bk) / 2.0 << " F1+2: " << getNumConClust(numCells, (ak + bk) / 2.0) << std::endl;
	};

	return (ak + bk) / 2.0;
}