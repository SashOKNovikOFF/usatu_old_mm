#include "printOut.hpp"
#include "Spline.hpp"

#include <ctime>
#include <cstdlib>
#include <cmath>
#include <vector>
#include <string>
#include <iostream>

const int numGisto = 1000;
const int M = 500000;
const int N = 50;

void task_1()
{
	std::vector<double> numbers;
	for (int i = 0; i < numGisto; i++)
	{
		double randNumber = (double)(rand()) / (double)(RAND_MAX);
		double lnNumber = -log(1.0 - randNumber);
		numbers.push_back(lnNumber);
	};

	printOutOnlyData("Output_1.txt", 3, numbers);
};

void task_2()
{
	std::vector<double> pointsX;
	std::vector<double> pointsY;

	double x = 0.0;
	double y = 0.0;

	for (int i = 0; i < N; i++)
	{
		double randNumber = (double)(rand()) / (double)(RAND_MAX);
		double lnNumber = -log(1.0 - randNumber);

		int direction = rand() % 4;
		switch (direction)
		{
		case 0: { x += lnNumber; break; };
		case 1: { x -= lnNumber; break; };
		case 2: { y += lnNumber; break; };
		case 3: { y -= lnNumber; break; };
		};

		pointsX.push_back(x);
		pointsY.push_back(y);
	};

	std::vector< std::vector<double> > points = { pointsX, pointsY };
	printOutWithoutHeader("Output_2.txt", 3, points, false);
};

void task_3()
{
	std::vector<double> radiuses;

	for (int i = 0; i < M; i++)
	{
		double x = 0.0;
		double y = 0.0;

		for (int j = 0; j < N; j++)
		{
			double randNumber = (double)(rand()) / (double)(RAND_MAX);
			double lnNumber = -log(1.0 - randNumber);

			while (randNumber == 1)
			{
				randNumber = (double)(rand()) / (double)(RAND_MAX);
				lnNumber = -log(1.0 - randNumber);
			};

			int direction = rand() % 4;
			switch (direction)
			{
			case 0: { x += lnNumber; break; };
			case 1: { x -= lnNumber; break; };
			case 2: { y += lnNumber; break; };
			case 3: { y -= lnNumber; break; };
			};
		};

		radiuses.push_back(sqrt(x * x + y * y));
	};

	printOutOnlyData("Output_3R.txt", 3, radiuses);

	std::vector<double> radiusProb;
	std::vector<double> probability;

	double maxElement = 0.0;
	for (std::size_t i = 0; i < radiuses.size(); i++)
		maxElement = (maxElement < radiuses[i]) ? radiuses[i] : maxElement;

	int numBins = 100;
	double step = maxElement / numBins;

	for (std::size_t i = 0; i < numBins; i++)
	{
		int count = 0;
		for (int j = 0; j < M; j++)
		if ((radiuses[j] > i * step) && (radiuses[j] <= (i + 1) * step))
			count++;

		probability.push_back((float)(count) / (float)(M));
		radiusProb.push_back((i + 0.5) * step);
	};

	std::vector< std::vector<double> > data = { radiusProb, probability };
	printOutWithoutHeader("Output_3P.txt", 3, data, false);
};

void task_4()
{
	//-----1. ���������� ��������

	std::vector<double> radiuses;

	for (int i = 0; i < M; i++)
	{
		double x = 0.0;
		double y = 0.0;

		for (int j = 0; j < N; j++)
		{
			double randNumber = (double)(rand()) / (double)(RAND_MAX);
			double lnNumber = -log(1.0 - randNumber);

			while (randNumber == 1)
			{
				randNumber = (double)(rand()) / (double)(RAND_MAX);
				lnNumber = -log(1.0 - randNumber);
			};

			int direction = rand() % 4;
			switch (direction)
			{
			case 0: { x += lnNumber; break; };
			case 1: { x -= lnNumber; break; };
			case 2: { y += lnNumber; break; };
			case 3: { y -= lnNumber; break; };
			};
		};

		radiuses.push_back(sqrt(x * x + y * y));
	};

	//-----2. ���������� ������������ "�� �����"

	std::vector<double> radiusProb;
	std::vector<double> probability;

	double maxElement = 0.0;
	for (std::size_t i = 0; i < radiuses.size(); i++)
		maxElement = (maxElement < radiuses[i]) ? radiuses[i] : maxElement;

	int numBins = 100;
	double step = maxElement / numBins;

	for (std::size_t i = 0; i < numBins; i++)
	{
		int count = 0;
		for (int j = 0; j < M; j++)
			if ((radiuses[j] > i * step) && (radiuses[j] <= (i + 1) * step))
				count++;
		
		probability.push_back((float)(count) / (float)(M));
		radiusProb.push_back((i + 0.5) * step);
	};

	//-----3. ���������� ������������ "� ������������ ��������"

	std::vector<double> probPoints;

	cubic_spline interpol;
	interpol.build_spline(radiusProb, probability, numBins);

	for (int i = 0; i < M; i++)
		probPoints.push_back(interpol.f(radiuses[i]));

	//-----4. ��������� �� ����� ��� �����������

	double sum = 0.0;
	for (int i = 0; i < M; i++)
		sum += probPoints[i];

	for (int i = 0; i < M; i++)
		probPoints[i] /= sum;

	//-----5. ������������ ������� ���������

	double rn   = 0.0;
	double r2n  = 0.0;
	double raf  = 0.0;
	double r2af = 0.0;
	for (int i = 0; i < M; i++)
	{
		rn += radiuses[i] * probPoints[i];
		r2n += radiuses[i] * radiuses[i] * probPoints[i];
		raf += radiuses[i];
		r2af += radiuses[i] * radiuses[i];
	};

	std::cout.precision(8);
	std::cout << "RN2: " << r2n - rn * rn << std::endl;
	std::cout << "RN2AF: " << r2af / M - raf * raf / M / M << std::endl;
};

int main()
{
	srand((unsigned int)time(NULL));
	
	task_1();
	task_2();
	task_3();
	task_4();

	return 0;
};