#include <SFML/Graphics.hpp>
#include "Model.hpp"
#include <iostream>

bool DEBUG = false;                          // ����� ���������� ����������
void debugOutput(int** grid, int numCells);  // ����� ������ �� ����� � �������
void debugState(State** grid, int numCells); // ����� ������ �� ���������� ���������� � �������
void debugTime(int tics);                    // ����� ������� � �������

int main()
{
	Model model(256, false);              // ������
	model.userInit();                     // ������������� ������
	int** grid = model.getGrid();         // ����� ����� ������ � "��������"
	int** eat = model.getFoodGrid();      // ����� ����� ������������� ������ � "��������"
	State** state = model.getStateGrid(); // ����� ����� ��������� ���������� � "��������"
	int numCells = model.getNumCells();   // ����� ������� ����� ������ � "��������"
	int tics = 0;                         // ���������� ������ � ������ ������� ������

	const int winSize = 800;    // ������ ����������� ����
	const float offset = 16.0f; // ������ ����� � ����
	const float recSize = 3.0f; // ������ �����

	// ������������� ����
	sf::RenderWindow window(sf::VideoMode(winSize, winSize), L"��������� ������� \"��������� - ����������� �����\"");

	// ���� ���� �������, ������� ����� �� �����
	while (window.isOpen())
	{
		if (DEBUG) debugTime(tics);             // ������� ����� � �������
		if (DEBUG) debugOutput(eat, numCells);  // ������� ����� ������������� � �������
		if (DEBUG) debugState(state, numCells); // ������� ����� ������������� � �������

		// ������������� �������
		sf::Event event;
		while (window.pollEvent(event)) // ���������� ������� "���� �������"
		{
			if (event.type == sf::Event::Closed)
				window.close();
		}

		window.clear(sf::Color::Black); // ������������� ���� ������� �����
		grid = model.getGrid();         // �������� ���������� ������ �� �����

		// ������������ ��������� ������ �����
		for (int x = 1; x <= numCells; x++)
			for (int y = 1; y <= numCells; y++)
				if (grid[x][y] == 1)
				{
					sf::RectangleShape rec(sf::Vector2f(recSize, recSize));
					//rec.setFillColor(sf::Color(255, 255, 255, (sf::Uint8)(grid[x][y] * 255)));
					rec.setOrigin(sf::Vector2f(-offset, -offset));
					rec.setPosition(sf::Vector2f((x - 1) * recSize, (y - 1) * recSize));
					window.draw(rec);
				};

		window.display();             // ������� ����� �� �����
		//sf::sleep(sf::seconds(0.5f)); // ����������� �����������
		tics++;                       // ����������� �����
		if (DEBUG) system("cls");     // ������� ����� �������

		model.calcEngine(); // ������� "���������"
	}

	return 0;
}

void debugOutput(int** grid, int numCells)
{
	std::cout << "------------" << std::endl;

	for (int x = 0; x < numCells + 2; x++)
	{
		for (int y = 0; y < numCells + 2; y++)
			std::cout << grid[x][y] << "\t";
		std::cout << std::endl;
	}
};

void debugState(State** grid, int numCells)
{
	std::cout << "------------" << std::endl;

	for (int x = 0; x < numCells + 2; x++)
	{
		for (int y = 0; y < numCells + 2; y++)
			std::cout << grid[x][y].energy << "---" << grid[x][y].timeLife << "\t";
		std::cout << std::endl;
	}
};

void debugTime(int tics)
{
	std::cout << "Time: " << tics << std::endl;
};