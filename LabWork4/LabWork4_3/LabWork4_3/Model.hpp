#pragma once

#include <ctime>
#include <cstdlib>
#include <vector>

// ������� �� �������:
// 1. ��������� ������� ���������� ����� ���������� ��������� �������.
// 2. ����� �� � ������� ���� ������������ ������������� ������ ��� �������? (����������� ����������)
// 3. � ����� ������ ��������� �������� ��� �������? (����������� ��������� �����������)

struct State; // "���������" ������
struct Point; // ���������� ������
class Model;  // ������ ���������� �������� "��������� - ����������� �����"

struct State
{
	State() // �����������
	{
		energy   = 0;
		timeLife = 0;
	};
	void clearState() // ������� ������
	{
		energy   = 0;
		timeLife = 0;
		moveBusy = 0;
	};

	int energy;   // ������� ������� ���������
	int timeLife; // ����� ����� ���������
	int moveBusy; // ����������-���� (���� �� ������ ������ ��� ����������� ���������)
};
struct Point
{
	// ������������
	Point() : x(0), y(0) { };
	Point(int x_, int y_) : x(x_), y(y_) { };

	int x; // ���������� x
	int y; // ���������� y
};

class Model
{
private:
	int numCells;      // ���������� ������
	int** orgGrid;     // ����� ������ ���������
	int** foodGrid;    // ����� ������������� ������
	State** stateGrid; // ����� "���������" ������

	int maxFoodValue;  // ������������ ������������� ������
	int deltaFood;     // ������� ������������� � ������
	int deltaEnergy;   // ������� ������ ������� ���������
	int maxEnergy;     // ������������ ����� ������� ���������
	int deltaExpens;   // ������� ������� ��������� �� ����
	int deltaDivision; // ������� ������� �� ������� ���������
	
	int timeLife;       // ����� ����� ��������� (� ������)
	int timeAdult;      // �����, � �������� ���������� ������� ��������� (� ������)
	float startOrgCoef; // �����������, ������������ ��������� ���������� ����������
	bool modification;  // ���� ����������� ������ �������� � ������ ������

	void clearOrg(int x, int y) // ������� ������ �� ���������
	{
		orgGrid[x][y] = 0;
		stateGrid[x][y].clearState();
	}
	bool deathOrg(int x, int y) // ���������, �� ���� �� ��������
	{
		// ���������, �� ������� �� ���� ��������
		if (stateGrid[x][y].timeLife == timeLife)
		{
			clearOrg(x, y);
			return true;
		};

		// ������ ������� ���������
		if (stateGrid[x][y].energy > deltaExpens)
			stateGrid[x][y].energy -= deltaExpens;
		else // ����� �������� ��������
		{
			clearOrg(x, y);
			return true;
		};

		return false;
	}
	void eatOrg(int x, int y) // �������� "��������"
	{
		// ���� ������������� ������ ���, ������� ��������� ���������
		if (foodGrid[x][y] >= deltaEnergy)
		{
			// ���� ������ ������� �� ������� ���������� �������
			bool fatOrg = ((stateGrid[x][y].energy + deltaEnergy) > maxEnergy) ? true : false;
			int food = (fatOrg) ? (maxEnergy - stateGrid[x][y].energy) : deltaEnergy;

			stateGrid[x][y].energy += food;
			foodGrid[x][y] = foodGrid[x][y] - food;
		}
		else // ����� ������� ��, ��� ��������
		{
			// ���� ������ ������� �� ������� ���������� �������
			bool fatOrg = ((stateGrid[x][y].energy + foodGrid[x][y]) > maxEnergy) ? true : false;
			int food = (fatOrg) ? (maxEnergy - stateGrid[x][y].energy) : foodGrid[x][y];

			stateGrid[x][y].energy += food;
			foodGrid[x][y] = 0;
		};
	}
	void divMoveOrg(int x, int y) // �������� ������������ ��� �������������
	{
		// ��������� ������� ��������� ���������� ������
		std::vector<Point> points;
		for (int xRel = -1; xRel <= 1; xRel++)
			for (int yRel = -1; yRel <= 1; yRel++)
				if (orgGrid[x + xRel][y + yRel] == 0)
					points.push_back(Point(x + xRel, y + yRel));

		// ���� ��������� ������ �������, ����������� ��� ���������� ��������
		if (points.size() != 0)
		{
			// ���� ������ ��� �������� � � �� ������� ������� ��� �����������
			if ((stateGrid[x][y].timeLife >= timeAdult) && (stateGrid[x][y].energy > deltaDivision))
			{
				// ��������� �������, ����� �������� "����������"
				stateGrid[x][y].energy -= deltaDivision;

				// ����������� �������� �� ������ ������ ��������� �������
				Point move = points[rand() % (points.size())];
				moveFromTo(Point(x, y), move);

				// ������ �������� � ������, � ������� ����������
				orgGrid[x][y] = 1;
				stateGrid[x][y].energy = deltaDivision;
				stateGrid[x][y].timeLife = 0;
			}
			else // ���������� �������� � ����������� �� ��������� �������
				moveOrg(points, x, y);
		};
	}
	void moveOrg(std::vector<Point> points, int x, int y)
	{
		if (!modification) // ���������� �������� � ����������� �� ��������� �������
		{
			// ����������� �������� �� ������ ������ ��������� �������
			Point move = points[rand() % (points.size())];
			moveFromTo(Point(x, y), move);
		}
		else
		{
			// ������� ��������� ������ � ������������ ��������������
			Point move = Point(x, y);
			for (std::size_t index = 0; index < points.size(); index++)
				if (foodGrid[move.x][move.y] < foodGrid[points[index].x][points[index].y])
					move = points[index];

			// ���� ����� ������ �������
			if (!((move.x == x) && (move.y == y)))
				moveFromTo(Point(x, y), move);
		};
	}
	void moveFromTo(Point from, Point to) // ����������� �������� �� ������ ������
	{
		// ��������� ����� ������, � ������� �������������
		orgGrid[to.x][to.y] = 1;
		stateGrid[to.x][to.y].energy = stateGrid[from.x][from.y].energy;
		stateGrid[to.x][to.y].timeLife = stateGrid[from.x][from.y].timeLife;
		stateGrid[to.x][to.y].moveBusy = 1;

		// ������� ������ ������, � ������� ����������
		clearOrg(from.x, from.y);
	}
	void updateGrids() // ��������� ������ �� ������ ����� "�������" ���������
	{
		for (int x = 1; x <= numCells; x++)
			for (int y = 1; y <= numCells; y++)
			{
				// ��������� ������ �� ����� �������������
				if ((foodGrid[x][y] + deltaFood) <= maxFoodValue)
					foodGrid[x][y] += deltaFood;

				// C��������� ����� ������������ ����������
				stateGrid[x][y].moveBusy = 0;
			};
	}

public:
	Model(int numCells_, bool modification_) // �����������
	{
		numCells = numCells_;
		modification = modification_;

		maxFoodValue = 10;
		deltaFood = 1;
		deltaEnergy = 5;
		maxEnergy = 35;
		deltaExpens = 2;
		deltaDivision = 3;

		timeLife = 15;
		timeAdult = 3;
		startOrgCoef = 0.3f;

		// ������������ ��������� ������ ��� ���������� ��������
		orgGrid = new int*[numCells + 2];
		foodGrid  = new int*[numCells + 2];
		stateGrid = new State*[numCells + 2];
		for (int index = 0; index < numCells + 2; index++)
		{
			orgGrid[index]   = new int[numCells + 2];
			foodGrid[index]  = new int[numCells + 2];
			stateGrid[index] = new State[numCells + 2];
		};

		// �������� �������
		for (int x = 0; x < numCells + 2; x++)
			for (int y = 0; y < numCells + 2; y++)
			{
				orgGrid[x][y] = 0;
				foodGrid[x][y] = 0;

				stateGrid[x][y].energy   = 0;
				stateGrid[x][y].timeLife = 0;
				stateGrid[x][y].moveBusy = 0;
			};

		// ������ ��������� ��������� �� �������
		for (int x = 0; x < numCells + 2; x++)
		{
			orgGrid[0][x] = 1;
			orgGrid[x][0] = 1;
			orgGrid[numCells + 1][x] = 1;
			orgGrid[x][numCells + 1] = 1;
		};

		srand((unsigned int)time(NULL)); // "���������� ������"
	}
	
	int calcEngine() // �������� ���������� �������� "��������� - ����������� �����"
	{
		// ����� �� ���� ������� � �����������
		for (int x = 1; x <= numCells; x++)
			for (int y = 1; y <= numCells; y++)
				if (orgGrid[x][y] == 1)
				{
					// ���������, �� ������������ �� �������� �����
					if (stateGrid[x][y].moveBusy == 1)
						continue;

					// ����������� ������� ���������
					if (stateGrid[x][y].timeLife < timeLife)
						stateGrid[x][y].timeLife++;

					// ���������, �� ���� �� ��������
					if (deathOrg(x, y))
						continue;

					// �������� ��������
					eatOrg(x, y);

					// �������� ������������ ��� �������������
					divMoveOrg(x, y);
				};

		// ��������� ������ �� ������ ����� "�������" ���������
		updateGrids();

		return 0;
	}

	int** getGrid() // �������� ��������� �� ������ ������ � �����������
	{
		return orgGrid;
	}
	int** getFoodGrid() // �������� ��������� �� ������ ������������� ������
	{
		return foodGrid;
	}
	State** getStateGrid() // �������� ��������� �� ������ ��������� ����������
	{
		return stateGrid;
	}
	int getNumCells() // �������� ������ �����
	{
		return numCells;
	}
	
	void randomInit() // ��������� ����� ��������� �������
	{
		int count = 0;
		for (int count = 0; count < (int)(startOrgCoef * numCells * numCells); count++)
		{
			int x = rand() % (numCells - 1) + 1;
			int y = rand() % (numCells - 1) + 1;
			while (orgGrid[x][y] == 1)
			{
				x = rand() % (numCells - 1) + 1;
				y = rand() % (numCells - 1) + 1;
			};
			
			orgGrid[x][y] = 1;
			stateGrid[x][y].energy = 10;
			stateGrid[x][y].timeLife = -1;
		};
		/*for (int x = 1; x <= numCells; x++)
			for (int y = 1; y <= numCells; y++)
			{
				//if (count <= )
				//{
					orgGrid[x][y] = rand() % 2;
					stateGrid[x][y].energy = (orgGrid[x][y] == 1) ? 10 : 0;
					stateGrid[x][y].timeLife = (orgGrid[x][y] == 1) ? -1 : 0;
				//}
				//else break;
				count += orgGrid[x][y];
			};*/
	}
	void userInit() // ������ ���������������� �����
	{
		int count = 0;
		for (int count = 0; count < (int)(startOrgCoef * 0.5 * numCells * numCells); count++)
		{
			int x = rand() % (numCells / 4 - 1) + 1;
			int y = rand() % (numCells - 1) + 1;
			while (orgGrid[x][y] == 1)
			{
				x = rand() % (numCells / 4 - 1) + 1;
				y = rand() % (numCells - 1) + 1;
			};

			orgGrid[x][y] = 1;
			stateGrid[x][y].energy = 10;
			stateGrid[x][y].timeLife = -1;
		};


		count = 0;
		for (int count = 0; count < (int)(startOrgCoef * 0.5 * numCells * numCells); count++)
		{
			int x = rand() % (numCells / 2 - 1) + numCells / 2;
			int y = rand() % (numCells - 1) + 1;
			while (orgGrid[x][y] == 1)
			{
				x = rand() % (numCells / 2 - 1) + numCells / 2;
				y = rand() % (numCells - 1) + 1;
			};

			orgGrid[x][y] = 1;
			stateGrid[x][y].energy = 10;
			stateGrid[x][y].timeLife = -1;
		};
	}
};