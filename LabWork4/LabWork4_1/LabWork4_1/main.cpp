#include <SFML/Graphics.hpp>
#include "Model.hpp"
#include <iostream>

void debugOutput(int** grid, int numCells); // ����� ������ �� ����� � �������

int main()
{
	Model model(256);                   // ������
	model.randomInit();                 // ������������� ������
	int** grid = model.getGrid();       // ����� ����� ������ � "��������"
	int numCells = model.getNumCells(); // ����� ������� ����� ������ � "��������"

	const int winSize = 800;    // ������ ����������� ����
	const float offset = 16.0f; // ������ ����� � ����
	const float recSize = 3.0f; // ������ �����

	// ������������� ����
	sf::RenderWindow window(sf::VideoMode(winSize, winSize), L"��������� ������� \"�����\"");

	// ���� ���� �������, ������� ����� �� �����
	while (window.isOpen())
	{
		// ������������� �������
		sf::Event event;
		while (window.pollEvent(event)) // ���������� ������� "���� �������"
		{
			if (event.type == sf::Event::Closed)
				window.close();
		}

		window.clear(sf::Color::Black); // ������������� ���� ������� �����
		grid = model.getGrid();         // �������� ���������� ������ �� �����

		// ������������ ��������� ������ �����
		for (int x = 1; x <= numCells; x++)
			for (int y = 1; y <= numCells; y++)
				if (grid[x][y] == 1)
				{
					sf::RectangleShape rec(sf::Vector2f(recSize, recSize));
					rec.setOrigin(sf::Vector2f(-offset, -offset));
					rec.setPosition(sf::Vector2f((x - 1) * recSize, (y - 1) * recSize));
					window.draw(rec);
				};

		window.display();             // ������� ����� �� �����
		sf::sleep(sf::seconds(0.5f)); // ����������� �����������
		
		model.calcEngine(); // ������� "���������"
	}

	return 0;
}

void debugOutput(int** grid, int numCells)
{
	for (int x = 0; x < numCells + 2; x++)
	{
		for (int y = 0; y < numCells + 2; y++)
			std::cout << grid[x][y] << " ";
		std::cout << std::endl;
	}
};