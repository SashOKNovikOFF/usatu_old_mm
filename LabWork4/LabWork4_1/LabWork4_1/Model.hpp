#pragma once

#include <ctime>
#include <cstdlib>

class Model; // ������ ���������� �������� "�����"

class Model
{
private:
	int numCells;  // ���������� ������
	int** oldGrid; // ����� ������� ���������
	int** newGrid; // ����� ������ ���������

public:
	Model(int numCells_) // �����������
	{
		numCells = numCells_;

		// ������������ ��������� ������ ��� ���������� ��������
		oldGrid = new int*[numCells + 2];
		newGrid = new int*[numCells + 2];
		for (int index = 0; index < numCells + 2; index++)
		{
			oldGrid[index] = new int[numCells + 2];
			newGrid[index] = new int[numCells + 2];
		};

		// �������� �������
		for (int x = 0; x < numCells + 2; x++)
			for (int y = 0; y < numCells + 2; y++)
			{
				oldGrid[x][y] = 0;
				newGrid[x][y] = 0;
			};
	}
	int calcEngine() // �������� ���������� �������� "�����"
	{
		// ����� �� ���� ������� ������� ���������
		for (int x = 1; x <= numCells; x++)
			for (int y = 1; y <= numCells; y++)
			{
				// ������� ��������� ������ ������ �������
				int counter = 0;
				counter += oldGrid[x - 1][y - 1] + oldGrid[x - 1][y    ] + oldGrid[x - 1][y + 1];
				counter += oldGrid[x    ][y - 1] + oldGrid[x    ][y + 1];
				counter += oldGrid[x + 1][y - 1] + oldGrid[x + 1][y    ] + oldGrid[x + 1][y + 1];

				// ����������, �������� �� ������ �������� ��� ���������
				/*if (counter == 3)
					newGrid[x][y] = 1;
				else if ((counter < 2) || (counter > 3))
					newGrid[x][y] = 0;
				else
					newGrid[x][y] = oldGrid[x][y];*/
				if (counter == 2)
					newGrid[x][y] = 1;
				else if (counter >= 3)
					newGrid[x][y] = 0;
				else
					newGrid[x][y] = oldGrid[x][y];
			};

		// ����� ������� � ������ ���������
		int** temp = oldGrid;
		oldGrid = newGrid;
		newGrid = temp;

		return 0;
	}
	int** getGrid() // �������� ��������� �� ������ ������
	{
		return oldGrid;
	}
	int getNumCells() // �������� ������ �����
	{
		return numCells;
	}
	void randomInit() // ��������� ����� ��������� �������
	{
		srand((unsigned int)time(NULL)); // "���������� ������"
		
		for (int x = 1; x <= numCells; x++)
			for (int y = 1; y <= numCells; y++)
				oldGrid[x][y] = rand() % 2;
	}
	void userInit() // ��������� �����, �������� "����������� �������"
	{
		oldGrid[1][1] = 1;
		oldGrid[4][4] = 1;
	}
	void gliderInit() // ������� �� ����� "�������"
	{
		oldGrid[1 + 10][2 + 10] = 1;
		oldGrid[2 + 10][3 + 10] = 1;
		oldGrid[3 + 10][1 + 10] = 1;
		oldGrid[3 + 10][2 + 10] = 1;
		oldGrid[3 + 10][3 + 10] = 1;
	}
};