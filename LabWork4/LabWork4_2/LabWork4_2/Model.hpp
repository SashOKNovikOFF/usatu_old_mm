#pragma once

#include <ctime>
#include <cstdlib>

struct State; // "���������" ������
class Model;  // ������ ���������� �������� "��������� ����"

struct State
{
	State() // �����������
	{
		numBeat = 0;
		numRecov = 0;
		startFlag = false;
	};

	int numBeat;    // ������� ����� ������ �����������
	int numRecov;   // ������� ����� ������ ��������������
	bool startFlag; // ����������-����, ������������, ����������� �� ������� ������
};
class Model
{
private:
	int numCells;       // ���������� ������
	float** oldGrid;    // ����� ������� ���������
	float** newGrid;    // ����� ������ ���������
	State** stateGrid;  // ����� "���������" ������

	float activBorder; // ����� ����������
	float procActiv;   // ������� �������� ������ ����������
	int numBeat;       // ����� ������ �����������
	int numRecov;      // ����� ������ ��������������

public:
	Model(int numCells_, float activBorder_, float procActiv_, int numBeat_, int numRecov_) // �����������
	{
		numCells = numCells_;
		activBorder = activBorder_;
		procActiv = procActiv_;
		numBeat = numBeat_;
		numRecov = numRecov_;

		// ������������ ��������� ������ ��� ���������� ��������
		oldGrid   = new float*[numCells + 2];
		newGrid   = new float*[numCells + 2];
		stateGrid = new State*[numCells + 2];
		for (int index = 0; index < numCells + 2; index++)
		{
			oldGrid[index]   = new float[numCells + 2];
			newGrid[index]   = new float[numCells + 2];
			stateGrid[index] = new State[numCells + 2];
		};

		// �������� �������
		for (int x = 0; x < numCells + 2; x++)
			for (int y = 0; y < numCells + 2; y++)
			{
				oldGrid[x][y] = 0;
				newGrid[x][y] = 0;

				stateGrid[x][y].numBeat   = 0;
				stateGrid[x][y].numRecov  = 0;
				stateGrid[x][y].startFlag = false;
			};

	}
	int calcEngine() // �������� ���������� �������� "��������� ����"
	{
		// ����� �� ���� ������� ������� ���������
		for (int x = 1; x <= numCells; x++)
			for (int y = 1; y <= numCells; y++)
			{
				if ((oldGrid[x][y] == 0.0f) && !stateGrid[x][y].startFlag) // ���� ������ ��������� � �����
				{
					// ������� ��������� �������� ��������� � ������� ������ �������
					float counter = 0;
					counter += oldGrid[x - 1][y - 1] + oldGrid[x - 1][y] + oldGrid[x - 1][y + 1];
					counter += oldGrid[x    ][y - 1] + oldGrid[x    ][y] + oldGrid[x    ][y + 1];
					counter += oldGrid[x + 1][y - 1] + oldGrid[x + 1][y] + oldGrid[x + 1][y + 1];

					// ����������, ����� �� ������ �����������
					if (counter >= activBorder)
					{
						newGrid[x][y] = 1.0f;
						stateGrid[x][y].startFlag = true;
					};
				}
				else if (stateGrid[x][y].numBeat < numBeat) // ���� ������ ��������� � ��������� �����������
				{
					newGrid[x][y] = 1.0f;
					stateGrid[x][y].numBeat++;
				}
				else if (stateGrid[x][y].numRecov < numRecov) // ���� ������ ��������� � ��������� ��������������
				{
					newGrid[x][y] = oldGrid[x][y] * (1.0f - procActiv);
					stateGrid[x][y].numRecov++;
				}
				else
				{
					// ����� ��������� ������
					stateGrid[x][y].numBeat = 0;
					stateGrid[x][y].numRecov = 0;

					// ������� ��������� �������� ��������� � ������� ������ �������
					float counter = 0;
					counter += oldGrid[x - 1][y - 1] + oldGrid[x - 1][y] + oldGrid[x - 1][y + 1];
					counter += oldGrid[x    ][y - 1] + oldGrid[x    ][y] + oldGrid[x    ][y + 1];
					counter += oldGrid[x + 1][y - 1] + oldGrid[x + 1][y] + oldGrid[x + 1][y + 1];

					// ����������, ����� �� ������ �����������
					if (counter >= activBorder)
						newGrid[x][y] = 1.0f;
					else
						newGrid[x][y] = oldGrid[x][y] * (1.0f - procActiv);
				};
			};

		// ����� ������� � ������ ���������
		float** temp = oldGrid;
		oldGrid = newGrid;
		newGrid = temp;

		return 0;
	}
	float** getGrid() // �������� ��������� �� ������ ������
	{
		return oldGrid;
	}
	int getNumCells() // �������� ������ �����
	{
		return numCells;
	}
	void randomInit() // ��������� ����� ��������� �������
	{
		srand((unsigned int)time(NULL)); // "���������� ������"

		for (int x = 1; x <= numCells; x++)
			for (int y = 1; y <= numCells; y++)
				oldGrid[x][y] = (float)(rand() % 2);
	}
	void userInit()
	{
		for (int x = 1; x <= numCells; x++)
		{
			oldGrid[x][           1] = 1.0f;
			oldGrid[x][numCells - 2] = 1.0f;
		};
	}
	void periodActiv() // ������ �������� ����������
	{
		for (int x = 10; x <= 13; x++)
			for (int y = 120; y <= 123; y++)
				oldGrid[x][y] = 1.0f;
	}
};