#include <SFML/Graphics.hpp>
#include "Model.hpp"
#include <iostream>

bool DEBUG = false;                           // ����������-���� ��� ������ ���������� ����������
void debugOutput(float** grid, int numCells); // ����� ������ �� ����� � �������
void debugPeriodic(int periodicTime);         // ����� �������, � ������� �������� ��� ������� �������� �����������

int main()
{
	Model model(256, 3.0f, 0.3f, 5, 8); // ������
	model.userInit();                   // ������������� ������
	float** grid = model.getGrid();     // ����� ����� ������ � "��������"
	int numCells = model.getNumCells(); // ����� ������� ����� ������ � "��������"
	int periodicTime = 0;               // ���������� ������ ��� ������� ���������� ������� (15 ������)

	const int winSize = 800;    // ������ ����������� ����
	const float offset = 16.0f; // ������ ����� � ����
	const float recSize = 3.0f; // ������ �����

	// ������������� ����
	sf::RenderWindow window(sf::VideoMode(winSize, winSize), L"��������� ������� \"��������� ����\"");

	// ���� ���� �������, ������� ����� �� �����
	while (window.isOpen())
	{
		if ((periodicTime % 15) == 0)
			model.periodActiv();
		periodicTime = (periodicTime == 15) ? 0 : periodicTime + 1;
		if (DEBUG) debugPeriodic(periodicTime);
		if (DEBUG) debugOutput(grid, numCells);

		// ������������� �������
		sf::Event event;
		while (window.pollEvent(event)) // ���������� ������� "���� �������"
		{
			if (event.type == sf::Event::Closed)
				window.close();
		}

		window.clear(sf::Color::Black); // ������������� ���� ������� �����
		grid = model.getGrid();         // �������� ���������� ������ �� �����

		// ������������ ��������� ������ �����
		for (int x = 1; x <= numCells; x++)
			for (int y = 1; y <= numCells; y++)
				if (grid[x][y] != 0.0f)
				{
					sf::RectangleShape rec(sf::Vector2f(recSize, recSize));
					rec.setFillColor(sf::Color(255, 255, 255, (sf::Uint8)(grid[x][y] * 255)));
					rec.setOrigin(sf::Vector2f(-offset, -offset));
					rec.setPosition(sf::Vector2f((x - 1) * recSize, (y - 1) * recSize));
					window.draw(rec);
				};

		window.display();             // ������� ����� �� �����
		//sf::sleep(sf::seconds(0.5f)); // ����������� �����������
		if (DEBUG) system("cls");     // ������� ����� �������

		model.calcEngine(); // ������� "���������"
	}

	return 0;
}

void debugOutput(float** grid, int numCells)
{
	std::cout << "------" << std::endl;

	for (int x = 0; x < numCells + 2; x++)
	{
		for (int y = 0; y < numCells + 2; y++)
			std::cout << grid[x][y] << "\t";
		std::cout << std::endl;
	}
};
void debugPeriodic(int periodicTime)
{
	std::cout << "Periodic Time: " << periodicTime;
};